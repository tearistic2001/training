package rs.netset.training;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-02-17T19:14:32+0100",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 19.0.1 (Oracle Corporation)"
)
@Component
public class CitizenMapperImpl implements CitizenMapper {

    @Autowired
    private ApplicationMapper applicationMapper;
    @Autowired
    private DocumentMapper documentMapper;

    @Override
    public CitizenDTO map(Citizen citizen) {
        if ( citizen == null ) {
            return null;
        }

        CitizenDTO citizenDTO = new CitizenDTO();

        citizenDTO.setFirstName( citizen.getFirstName() );
        citizenDTO.setLastName( citizen.getLastName() );
        citizenDTO.setEmail( citizen.getEmail() );
        citizenDTO.setUmcn( citizen.getUmcn() );
        citizenDTO.setCitizenDocuments( documentListToDocumentDTOList( citizen.getCitizenDocuments() ) );
        citizenDTO.setCitizenApplications( applicationListToApplicationDTOList( citizen.getCitizenApplications() ) );

        return citizenDTO;
    }

    @Override
    public Citizen map(CitizenDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Citizen citizen = new Citizen();

        citizen.setFirstName( dto.getFirstName() );
        citizen.setLastName( dto.getLastName() );
        citizen.setEmail( dto.getEmail() );
        citizen.setUmcn( dto.getUmcn() );
        citizen.setCitizenDocuments( documentDTOListToDocumentList( dto.getCitizenDocuments() ) );
        citizen.setCitizenApplications( applicationDTOListToApplicationList( dto.getCitizenApplications() ) );

        return citizen;
    }

    protected List<DocumentDTO> documentListToDocumentDTOList(List<Document> list) {
        if ( list == null ) {
            return null;
        }

        List<DocumentDTO> list1 = new ArrayList<DocumentDTO>( list.size() );
        for ( Document document : list ) {
            list1.add( documentMapper.map( document ) );
        }

        return list1;
    }

    protected List<ApplicationDTO> applicationListToApplicationDTOList(List<Application> list) {
        if ( list == null ) {
            return null;
        }

        List<ApplicationDTO> list1 = new ArrayList<ApplicationDTO>( list.size() );
        for ( Application application : list ) {
            list1.add( applicationMapper.map( application ) );
        }

        return list1;
    }

    protected List<Document> documentDTOListToDocumentList(List<DocumentDTO> list) {
        if ( list == null ) {
            return null;
        }

        List<Document> list1 = new ArrayList<Document>( list.size() );
        for ( DocumentDTO documentDTO : list ) {
            list1.add( documentMapper.map( documentDTO ) );
        }

        return list1;
    }

    protected List<Application> applicationDTOListToApplicationList(List<ApplicationDTO> list) {
        if ( list == null ) {
            return null;
        }

        List<Application> list1 = new ArrayList<Application>( list.size() );
        for ( ApplicationDTO applicationDTO : list ) {
            list1.add( applicationMapper.map( applicationDTO ) );
        }

        return list1;
    }
}
