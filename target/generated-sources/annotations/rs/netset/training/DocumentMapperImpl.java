package rs.netset.training;

import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-02-17T19:14:32+0100",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 19.0.1 (Oracle Corporation)"
)
@Component
public class DocumentMapperImpl implements DocumentMapper {

    @Override
    public DocumentDTO map(Document document) {
        if ( document == null ) {
            return null;
        }

        if (document instanceof ID) {
            return map( (ID) document );
        }
        else if (document instanceof Passport) {
            return map( (Passport) document );
        }
        else {
            throw new IllegalArgumentException("Not all subclasses are supported for this mapping. Missing for " + document.getClass());
        }
    }

    @Override
    public Document map(DocumentDTO dto) {
        if ( dto == null ) {
            return null;
        }

        if (dto instanceof IDDTO) {
            return map( (IDDTO) dto );
        }
        else if (dto instanceof PassportDTO) {
            return map( (PassportDTO) dto );
        }
        else {
            throw new IllegalArgumentException("Not all subclasses are supported for this mapping. Missing for " + dto.getClass());
        }
    }

    @Override
    public IDDTO map(ID id) {
        if ( id == null ) {
            return null;
        }

        IDDTO iDDTO = new IDDTO();

        iDDTO.setDocumentID( id.getDocumentID() );
        iDDTO.setFirstName( id.getFirstName() );
        iDDTO.setLastName( id.getLastName() );
        iDDTO.setDateOfBirth( id.getDateOfBirth() );
        iDDTO.setUmcn( id.getUmcn() );
        iDDTO.setValidFrom( id.getValidFrom() );
        iDDTO.setValidTo( id.getValidTo() );
        iDDTO.setState( id.getState() );

        return iDDTO;
    }

    @Override
    public ID map(IDDTO dto) {
        if ( dto == null ) {
            return null;
        }

        ID iD = new ID();

        iD.setDocumentID( dto.getDocumentID() );
        iD.setFirstName( dto.getFirstName() );
        iD.setLastName( dto.getLastName() );
        iD.setDateOfBirth( dto.getDateOfBirth() );
        iD.setUmcn( dto.getUmcn() );
        iD.setValidFrom( dto.getValidFrom() );
        iD.setValidTo( dto.getValidTo() );
        iD.setState( dto.getState() );

        return iD;
    }

    @Override
    public PassportDTO map(Passport passport) {
        if ( passport == null ) {
            return null;
        }

        PassportDTO passportDTO = new PassportDTO();

        passportDTO.setDocumentID( passport.getDocumentID() );
        passportDTO.setFirstName( passport.getFirstName() );
        passportDTO.setLastName( passport.getLastName() );
        passportDTO.setDateOfBirth( passport.getDateOfBirth() );
        passportDTO.setUmcn( passport.getUmcn() );
        passportDTO.setValidFrom( passport.getValidFrom() );
        passportDTO.setValidTo( passport.getValidTo() );
        passportDTO.setState( passport.getState() );
        passportDTO.passportNumber = passport.passportNumber;
        passportDTO.countryOfIssuance = passport.countryOfIssuance;
        passportDTO.placeOfResidence = passport.placeOfResidence;

        return passportDTO;
    }

    @Override
    public Passport map(PassportDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Passport passport = new Passport();

        passport.setDocumentID( dto.getDocumentID() );
        passport.setFirstName( dto.getFirstName() );
        passport.setLastName( dto.getLastName() );
        passport.setDateOfBirth( dto.getDateOfBirth() );
        passport.setUmcn( dto.getUmcn() );
        passport.setValidFrom( dto.getValidFrom() );
        passport.setValidTo( dto.getValidTo() );
        passport.setState( dto.getState() );
        passport.passportNumber = dto.passportNumber;
        passport.countryOfIssuance = dto.countryOfIssuance;
        passport.placeOfResidence = dto.placeOfResidence;

        return passport;
    }
}
