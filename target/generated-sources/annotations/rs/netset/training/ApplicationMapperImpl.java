package rs.netset.training;

import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-02-17T19:14:32+0100",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 19.0.1 (Oracle Corporation)"
)
@Component
public class ApplicationMapperImpl implements ApplicationMapper {

    @Override
    public ApplicationDTO map(Application application) {
        if ( application == null ) {
            return null;
        }

        ApplicationDTO applicationDTO = new ApplicationDTO();

        applicationDTO.setApplicationId( application.getApplicationId() );
        applicationDTO.setDocumentType( application.getDocumentType() );
        applicationDTO.setState( application.getState() );
        applicationDTO.setFirstName( application.getFirstName() );
        applicationDTO.setLastName( application.getLastName() );
        applicationDTO.setUmcn( application.getUmcn() );
        applicationDTO.setDateOfBirth( application.getDateOfBirth() );
        applicationDTO.setPlaceOfResidence( application.getPlaceOfResidence() );
        applicationDTO.setEmail( application.getEmail() );

        return applicationDTO;
    }

    @Override
    public Application map(ApplicationDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Application application = new Application();

        application.setApplicationId( dto.getApplicationId() );
        application.setDocumentType( dto.getDocumentType() );
        application.setState( dto.getState() );
        application.setFirstName( dto.getFirstName() );
        application.setLastName( dto.getLastName() );
        application.setUmcn( dto.getUmcn() );
        application.setDateOfBirth( dto.getDateOfBirth() );
        application.setPlaceOfResidence( dto.getPlaceOfResidence() );
        application.setEmail( dto.getEmail() );

        return application;
    }
}
