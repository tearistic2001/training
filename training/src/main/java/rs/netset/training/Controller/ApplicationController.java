package rs.netset.training.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import rs.netset.training.Service.ApplicationService;
import rs.netset.training.DTO.ApplicationDTO;

import java.util.List;

@RestController
@RequestMapping("/applications")
public class ApplicationController {

    @Autowired
    private ApplicationService applicationService;

    @GetMapping()
    public List<ApplicationDTO> getAllApplications() {
        return applicationService.getAll();
    }


    @GetMapping("/{id}")
    public ApplicationDTO getApplications(@PathVariable String id) {
        return applicationService.get(id);
    }

    @PostMapping()
    public void addApplication(@RequestBody ApplicationDTO applicationDTO) {
        applicationService.add(applicationDTO);
    }

    @PutMapping("/{id}")
    public void updateApplication(@RequestBody ApplicationDTO applicationDTO, @PathVariable String id){
        applicationService.update(applicationDTO,id);
    }

    @PutMapping("/{id}/reject")
    public void rejectApplication(@PathVariable String id){
        applicationService.reject(id);
    }

    @PutMapping("/{id}/approve")
    public void approveApplication(@PathVariable String id){
        applicationService.approve(id);
    }

    @DeleteMapping("/{id}")
    public void deleteApplication(@PathVariable String id) {
        applicationService.delete(id);
    }
}
