package rs.netset.training.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import rs.netset.training.DTO.DocumentDTO;
import rs.netset.training.Service.DocumentService;

import java.util.List;

@RestController
@RequestMapping("/documents")
public class DocumentController {

    @Autowired
    private DocumentService documentService;

    @GetMapping()
    public List<DocumentDTO> getAllDocuments() {
        return documentService.getAll();
    }

    @GetMapping("/{id}")
    public DocumentDTO getDocument(@PathVariable String id) {
        return documentService.get(id);
    }

    @PostMapping()
    public void addDocument(@RequestBody DocumentDTO documentDTO) {
        documentService.add(documentDTO);
    }

    @PutMapping("/{id}")
    public void updateDocument(@RequestBody DocumentDTO documentDTO, @PathVariable String id){
        documentService.update(documentDTO,id);
    }

    @DeleteMapping("/{id}")
    public void deleteDocument(@PathVariable String id) {
        documentService.delete(id);
    }
}
