package rs.netset.training.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import rs.netset.training.Service.CitizenService;
import rs.netset.training.DTO.ApplicationDTO;
import rs.netset.training.DTO.CitizenDTO;
import rs.netset.training.DTO.DocumentDTO;

import java.util.List;

@RestController
@RequestMapping("/citizens")
public class CitizenController {

    @Autowired
    private CitizenService citizenService;

    @GetMapping()
    public List<CitizenDTO> getAllCitizens() {
        return citizenService.getAll();
    }

    @GetMapping("/{id}")
    public CitizenDTO getCitizen(@PathVariable String id) {
        return citizenService.get(id);
    }

    @GetMapping("/{id}/documents")
    public List<DocumentDTO> getCitizenDocuments(@PathVariable String id) {
        return citizenService.getDocuments(id);
    }

    @GetMapping("/{id}/applications")
    public List<ApplicationDTO> getCitizenApplications(@PathVariable String id) {
        return citizenService.getApplications(id);
    }

    @PostMapping()
    public void addCitizen(@RequestBody CitizenDTO citizenDTO) {
        citizenService.add(citizenDTO);
    }

    @PutMapping("/{id}")
    public void updateCitizen(@RequestBody CitizenDTO citizenDTO, @PathVariable String id){
        citizenService.update(citizenDTO,id);
    }

    @DeleteMapping("/{id}")
    public void deleteCitizen(@PathVariable String id) {
        citizenService.delete(id);
    }
}
