package rs.netset.training;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import rs.netset.training.DTO.ApplicationDTO;
import rs.netset.training.Entity.Application;

@Mapper(componentModel = "spring")
public interface ApplicationMapper {

    ApplicationDTO map(Application application);

    @Mapping(target = "citizenA", ignore = true)
    Application map(ApplicationDTO dto);
}
