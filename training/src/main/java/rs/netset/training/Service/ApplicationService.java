package rs.netset.training.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.netset.training.ApplicationMapper;
import rs.netset.training.DTO.ApplicationDTO;
import rs.netset.training.Entity.Application;
import rs.netset.training.Repository.ApplicationRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class ApplicationService {

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private ApplicationMapper applicationMapper;

    public List<ApplicationDTO> getAll() {
        List<Application> applications = applicationRepository.findAll();
        List<ApplicationDTO> dtos = new ArrayList<>();
        for(Application app : applications) {
            dtos.add(applicationMapper.map(app));
        }
        return dtos;
    }

    public ApplicationDTO get(String id) {
        Application application = applicationRepository.findApplicationByApplicationId(id);
        return applicationMapper.map(application);
    }

    public void add(ApplicationDTO applicationDTO) {
        Application application = applicationMapper.map(applicationDTO);
        application.setApplicationId(UUID.randomUUID().toString());
        application.setState(Application.State.ENROLMENT);
        applicationRepository.save(application);
    }
    public void update(ApplicationDTO applicationDTO, String id) {
        Application application = applicationMapper.map(applicationDTO);
        application.setApplicationId(id);
        application.setState(Application.State.REVIEW);
        applicationRepository.save(application);
    }
    public void delete(String id) {
        Application application = applicationRepository.findById(id).orElseThrow();
        if (application.getState() == Application.State.ENROLMENT) {
            applicationRepository.deleteById(id);
        }
    }


    public void reject(String id) {
        Application application = applicationRepository.findById(id).orElseThrow();
        if (application.getState() == Application.State.REVIEW) {
            application.setState(Application.State.REJECTED);
        }
        applicationRepository.save(application);
    }

    public void approve(String id) {
        Application application = applicationRepository.findById(id).orElseThrow();
        if (application.getState() == Application.State.REVIEW) {
            application.setState(Application.State.APPROVED);
        }
        applicationRepository.save(application);
    }
}
