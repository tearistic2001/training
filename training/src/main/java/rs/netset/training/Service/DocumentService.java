package rs.netset.training.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.netset.training.DTO.DocumentDTO;
import rs.netset.training.DocumentMapper;
import rs.netset.training.Entity.Document;
import rs.netset.training.Repository.DocumentRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@Service
public class DocumentService {

    @Autowired
    private DocumentRepository documentRepository;

    @Autowired
    private DocumentMapper documentMapper;

    public List<DocumentDTO> getAll() {
        List<Document> documents = documentRepository.findAll();
        List<DocumentDTO> dtos = new ArrayList<>();
        for(Document doc : documents) {
            dtos.add(documentMapper.map(doc));
        }
        return dtos;
    }

    public DocumentDTO get(String id) {
        Document document = documentRepository.findDocumentByDocumentID(id);
        return documentMapper.map(document);
    }


    public void add(DocumentDTO documentDTO) {
        Document document = documentMapper.map(documentDTO);
        document.setDocumentID(UUID.randomUUID().toString());
        document.setState(Document.DocumentState.PERSONALIZING);
        documentRepository.save(document);
    }
    public void update(DocumentDTO documentDTO, String id) {
        Document document = documentMapper.map(documentDTO);
        document.setDocumentID(id);
        documentRepository.save(document);
    }
    public void delete(String id) {
        documentRepository.deleteById(id);
    }


}
