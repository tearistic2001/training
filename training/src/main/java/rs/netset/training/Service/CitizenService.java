package rs.netset.training.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.netset.training.ApplicationMapper;
import rs.netset.training.CitizenMapper;
import rs.netset.training.DTO.ApplicationDTO;
import rs.netset.training.DTO.CitizenDTO;
import rs.netset.training.DTO.DocumentDTO;
import rs.netset.training.DocumentMapper;
import rs.netset.training.Entity.Application;
import rs.netset.training.Entity.Citizen;
import rs.netset.training.Entity.Document;
import rs.netset.training.Repository.CitizenRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class CitizenService {
    @Autowired
    private CitizenRepository citizenRepository;

    @Autowired
    private CitizenMapper citizenMapper;

    @Autowired
    private DocumentMapper documentMapper;

    @Autowired
    private ApplicationMapper applicationMapper;


    public List<CitizenDTO> getAll() {
        List<Citizen> citizens = citizenRepository.findAll();
        List<CitizenDTO> dtos = new ArrayList<>();
        for(Citizen cit : citizens) {
            dtos.add(citizenMapper.map(cit));
        }
        return dtos;
    }
    public CitizenDTO get(String id) {
        Citizen citizen = citizenRepository.findCitizenByUmcn(id);
        return citizenMapper.map(citizen);
    }

    public void add(CitizenDTO citizenDTO) {
        Citizen citizen = citizenMapper.map(citizenDTO);
        citizenRepository.save(citizen);
    }
    public void update(CitizenDTO citizenDTO, String id) {
        Citizen citizen = citizenMapper.map(citizenDTO);
        citizenRepository.save(citizen);
    }
    public void delete(String id) {
        citizenRepository.deleteById(id);
    }

    public List<DocumentDTO> getDocuments(String umcn) {
        List<Document> documents = citizenRepository.findCitizenByUmcn(umcn).getCitizenDocuments();
        List<DocumentDTO> dtos = new ArrayList<>();
        for(Document doc : documents) {
            dtos.add(documentMapper.map(doc));
        }
        return dtos;
    }

    public List<ApplicationDTO> getApplications(String umcn) {
        List<Application> applications = citizenRepository.findCitizenByUmcn(umcn).getCitizenApplications();
        List<ApplicationDTO> dtos = new ArrayList<>();
        for(Application app : applications) {
            dtos.add(applicationMapper.map(app));
        }
        return dtos;
    }
}
