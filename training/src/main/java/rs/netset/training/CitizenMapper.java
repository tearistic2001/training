package rs.netset.training;

import org.mapstruct.Mapper;
import rs.netset.training.DTO.CitizenDTO;
import rs.netset.training.Entity.Citizen;

@Mapper(componentModel = "spring", uses = {ApplicationMapper.class, DocumentMapper.class})
public interface CitizenMapper {

    CitizenDTO map(Citizen citizen);

    Citizen map(CitizenDTO dto);
}
