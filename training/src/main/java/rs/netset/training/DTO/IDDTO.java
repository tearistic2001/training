package rs.netset.training.DTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IDDTO extends DocumentDTO{

    private String idNumber;
}
