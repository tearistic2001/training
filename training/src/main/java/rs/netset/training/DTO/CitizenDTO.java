package rs.netset.training.DTO;

import java.util.List;


public class CitizenDTO {

    private String firstName;
    private String lastName;
    private String email;
    private String umcn;

    private List<DocumentDTO> citizenDocuments;
    private List<ApplicationDTO> citizenApplications;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUmcn() {
        return umcn;
    }

    public void setUmcn(String umcn) {
        this.umcn = umcn;
    }

    public List<DocumentDTO> getCitizenDocuments() {
        return citizenDocuments;
    }

    public void setCitizenDocuments(List<DocumentDTO> citizenDocuments) {
        this.citizenDocuments = citizenDocuments;
    }

    public List<ApplicationDTO> getCitizenApplications() {
        return citizenApplications;
    }

    public void setCitizenApplications(List<ApplicationDTO> citizenApplications) {
        this.citizenApplications = citizenApplications;
    }
}
