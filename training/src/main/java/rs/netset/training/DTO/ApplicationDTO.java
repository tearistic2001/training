package rs.netset.training.DTO;


import rs.netset.training.Entity.Application;

import java.time.LocalDate;


public class ApplicationDTO {

    private String applicationId;
    private Application.DocumentType documentType;
    private Application.State state;
    private String firstName;
    private String lastName;
    private String umcn;
    private LocalDate dateOfBirth; //id
    private String placeOfResidence; //pass
    private String email;


    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public Application.DocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(Application.DocumentType documentType) {
        this.documentType = documentType;
    }

    public Application.State getState() {
        return state;
    }

    public void setState(Application.State state) {
        this.state = state;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUmcn() {
        return umcn;
    }

    public void setUmcn(String umcn) {
        this.umcn = umcn;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPlaceOfResidence() {
        return placeOfResidence;
    }

    public void setPlaceOfResidence(String placeOfResidence) {
        this.placeOfResidence = placeOfResidence;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
