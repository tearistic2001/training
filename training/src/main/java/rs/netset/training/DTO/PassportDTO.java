package rs.netset.training.DTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PassportDTO extends DocumentDTO{

    public String passportNumber;
    public String countryOfIssuance;
    public String placeOfResidence;

}
