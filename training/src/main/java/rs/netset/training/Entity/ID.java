package rs.netset.training.Entity;

import jakarta.persistence.Entity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class ID extends Document {
    private String idNumber;

}
