package rs.netset.training.Entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;

import lombok.ToString;

import java.util.List;

@Entity
public class Citizen {

    private String firstName;
    private String lastName;
    private String email;

    @Id
    @Column(name = "umcn", nullable=false)
    @ToString.Exclude
    private String umcn;


    @JsonManagedReference
    @OneToMany(mappedBy = "citizenD",fetch = FetchType.LAZY)
    @ToString.Exclude
    private List<Document> citizenDocuments;


    @JsonManagedReference
    @OneToMany(mappedBy = "citizenA",fetch = FetchType.LAZY)
    @ToString.Exclude
    private List<Application> citizenApplications;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUmcn() {
        return umcn;
    }

    public void setUmcn(String umcn) {
        this.umcn = umcn;
    }

    public List<Document> getCitizenDocuments() {
        return citizenDocuments;
    }

    public void setCitizenDocuments(List<Document> citizenDocuments) {
        this.citizenDocuments = citizenDocuments;
    }

    public List<Application> getCitizenApplications() {
        return citizenApplications;
    }

    public void setCitizenApplications(List<Application> citizenApplications) {
        this.citizenApplications = citizenApplications;
    }
}
