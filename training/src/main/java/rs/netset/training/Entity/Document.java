package rs.netset.training.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import jakarta.persistence.*;
import lombok.ToString;

import java.time.LocalDate;

@Entity
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({@JsonSubTypes.Type(value = ID.class, name = "ID"),
        @JsonSubTypes.Type(value = Passport.class, name = "Passport")})
public abstract class Document {

    public enum DocumentState {
        PERSONALIZING, NOT_ISSSUED, ISSUED, REVOKED
    } ;

    @Id
    private String documentID;

    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;
    @Column(name = "umcn", nullable=false)
    private String umcn;
    private LocalDate validFrom;
    private LocalDate validTo;
    private DocumentState state;

    @JsonBackReference
    @ManyToOne(optional=false)
    @JoinColumn(name = "umcn",insertable=false, updatable=false)
    @ToString.Exclude
    private Citizen citizenD;

    public String getDocumentID() {
        return documentID;
    }

    public void setDocumentID(String documentID) {
        this.documentID = documentID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getUmcn() {
        return umcn;
    }

    public void setUmcn(String umcn) {
        this.umcn = umcn;
    }

    public LocalDate getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(LocalDate validFrom) {
        this.validFrom = validFrom;
    }

    public LocalDate getValidTo() {
        return validTo;
    }

    public void setValidTo(LocalDate validTo) {
        this.validTo = validTo;
    }

    public DocumentState getState() {
        return state;
    }

    public void setState(DocumentState state) {
        this.state = state;
    }

    public Citizen getCitizenD() {
        return citizenD;
    }

    public void setCitizenD(Citizen citizenD) {
        this.citizenD = citizenD;
    }
}
