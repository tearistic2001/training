package rs.netset.training.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.ToString;

import java.time.LocalDate;


@Entity
public class Application {
    public enum DocumentType {
        ID, Passport
    };

    public enum State {
        ENROLMENT, REVIEW, APPROVED, REJECTED
    };

    @Id
    private String applicationId;

    private DocumentType documentType;
    private State state;
    private String firstName;
    private String lastName;
    @Column(name = "umcn", nullable=false)
    private String umcn;
    private LocalDate dateOfBirth; //id
    private String placeOfResidence; //pass
    private String email;

    @JsonBackReference
    @ManyToOne(optional=false)
    @JoinColumn(name = "umcn",insertable=false, updatable=false)
    @ToString.Exclude
    private Citizen citizenA;


    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public DocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentType documentType) {
        this.documentType = documentType;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUmcn() {
        return umcn;
    }

    public void setUmcn(String umcn) {
        this.umcn = umcn;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPlaceOfResidence() {
        return placeOfResidence;
    }

    public void setPlaceOfResidence(String placeOfResidence) {
        this.placeOfResidence = placeOfResidence;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Citizen getCitizenA() {
        return citizenA;
    }

    public void setCitizenA(Citizen citizenA) {
        this.citizenA = citizenA;
    }



}
