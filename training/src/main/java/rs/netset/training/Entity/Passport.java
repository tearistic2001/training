package rs.netset.training.Entity;

import jakarta.persistence.Entity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Passport extends Document {
    public String passportNumber;
    public String countryOfIssuance;
    public String placeOfResidence;

}
