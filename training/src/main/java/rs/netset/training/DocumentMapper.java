package rs.netset.training;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.SubclassExhaustiveStrategy;
import org.mapstruct.SubclassMapping;
import rs.netset.training.DTO.DocumentDTO;
import rs.netset.training.DTO.IDDTO;
import rs.netset.training.DTO.PassportDTO;
import rs.netset.training.Entity.Document;
import rs.netset.training.Entity.ID;
import rs.netset.training.Entity.Passport;

@Mapper( componentModel = "spring",subclassExhaustiveStrategy = SubclassExhaustiveStrategy.RUNTIME_EXCEPTION)
public interface DocumentMapper {

    @SubclassMapping(source = ID.class, target = IDDTO.class)
    @SubclassMapping(source = Passport.class, target = PassportDTO.class)
    DocumentDTO map(Document document);

    @SubclassMapping(source = IDDTO.class, target = ID.class)
    @SubclassMapping(source = PassportDTO.class, target = Passport.class)
    @Mapping(target = "citizenD", ignore = true)
    Document map(DocumentDTO dto);

    IDDTO map(ID id);

    ID map(IDDTO dto);

    PassportDTO map(Passport passport);

    Passport map(PassportDTO dto);

}