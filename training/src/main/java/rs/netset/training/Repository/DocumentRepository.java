package rs.netset.training.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import rs.netset.training.Entity.Document;

import java.util.List;


@Repository
public interface DocumentRepository extends CrudRepository<Document, String> {
    Document findDocumentByDocumentID(String id);
    List<Document> findAll();

}
