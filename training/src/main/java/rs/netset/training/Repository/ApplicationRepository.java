package rs.netset.training.Repository;

import org.springframework.data.repository.CrudRepository;
import rs.netset.training.Entity.Application;

import java.util.List;

public interface ApplicationRepository extends CrudRepository<Application, String> {
    Application findApplicationByApplicationId(String id);
    List<Application> findAll();
}

