package rs.netset.training.Repository;

import org.springframework.data.repository.CrudRepository;
import rs.netset.training.Entity.Citizen;

import java.util.List;

public interface CitizenRepository extends CrudRepository<Citizen, String> {
    Citizen findCitizenByUmcn(String umcn);
    List<Citizen> findAll();
}
