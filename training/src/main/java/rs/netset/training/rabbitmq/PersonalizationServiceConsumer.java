package rs.netset.training.rabbitmq;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;
import rs.netset.training.DTO.DocumentDTO;

@Service
public class PersonalizationServiceConsumer {
    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(PersonalizationServiceConsumer.class);

    @RabbitListener(queues = "${rabbitmq.queue.json.name}")
    public void consumeJsonMessage(DocumentDTO document) {
        log.info(String.format("Received JSON message: %s",document.toString()));
    }
}
