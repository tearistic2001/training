package rs.netset.training.rabbitmq;

public class Message {

    public enum State {
        SUCCESSFUL, NOT_SUCCESSFUL
    };

    private String id;
    private State status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public State getStatus() {
        return status;
    }

    public void setStatus(State status) {
        this.status = status;
    }
}
