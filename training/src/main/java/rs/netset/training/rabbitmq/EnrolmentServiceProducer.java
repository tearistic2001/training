package rs.netset.training.rabbitmq;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import rs.netset.training.DTO.DocumentDTO;

@Service
public class EnrolmentServiceProducer {

    @Value("${rabbitmq.exchange.name}")
    private String exchange;

    @Value("${rabbitmq.json.routing.key}")
    private String jsonRoutingKey;

    private RabbitTemplate rabbitTemplate;

    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(EnrolmentServiceProducer.class);

    public EnrolmentServiceProducer(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendJsonMessage(DocumentDTO document) {
        log.info(String.format("Json message sent: %s", document.toString()));
        rabbitTemplate.convertAndSend(exchange, jsonRoutingKey, document);
    }

}
