package rs.netset.training.rabbitmq;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.netset.training.DTO.DocumentDTO;


@RestController
@RequestMapping("/documents/personalize")
public class MessageJsonController {
    private EnrolmentServiceProducer jsonProducer;

    public MessageJsonController(EnrolmentServiceProducer jsonProducer) {
        this.jsonProducer = jsonProducer;
    }

    @PostMapping
    public ResponseEntity<String> sendJsonMessage(@RequestBody DocumentDTO document) {
        jsonProducer.sendJsonMessage(document);
        return ResponseEntity.ok("Json message sent");
    }
}
