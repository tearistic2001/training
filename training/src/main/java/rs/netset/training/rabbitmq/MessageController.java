package rs.netset.training.rabbitmq;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/documents/personalized")
public class MessageController {

    private PersonalizationServiceProducer producer;

    public MessageController(PersonalizationServiceProducer producer) {
        this.producer = producer;
    }

    @PostMapping
    public ResponseEntity<String> sendMessage(@RequestBody Message message) {
        producer.sendMessage(message);
        return ResponseEntity.ok("Message sent");
    }
}
