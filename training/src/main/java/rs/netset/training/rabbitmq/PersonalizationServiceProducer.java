package rs.netset.training.rabbitmq;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class PersonalizationServiceProducer {

    private int docs = 0;
    private int personalizedDocs = 0;

    @Value("${rabbitmq.exchange.name}")
    private String exchange;

    @Value("${rabbitmq.routing.key}")
    private String routingKey;

    private RabbitTemplate rabbitTemplate;

    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(PersonalizationServiceProducer.class);

    public PersonalizationServiceProducer(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendMessage(Message message) {
        log.info(String.format("Message sent: %s",message.toString()));
        rabbitTemplate.convertAndSend(exchange, routingKey, message);
        docs++;
        if (message.getStatus() == Message.State.SUCCESSFUL) {
            personalizedDocs++;
        }

    }


}
