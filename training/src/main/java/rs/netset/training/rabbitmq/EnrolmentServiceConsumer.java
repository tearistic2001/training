package rs.netset.training.rabbitmq;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.netset.training.Entity.Document;
import rs.netset.training.Repository.DocumentRepository;


@Service
public class EnrolmentServiceConsumer {

    @Autowired
    private DocumentRepository documentRepository;

    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(EnrolmentServiceConsumer.class);

    @RabbitListener(queues = "${rabbitmq.queue.name}")
    public void consume(Message message) {
        log.info(String.format("Received message: %s",message.toString()));
        if (message.getStatus() == Message.State.SUCCESSFUL) {
            String id = message.getId();
            Document document = documentRepository.findDocumentByDocumentID(id);
            if (document != null) {
                document.setState(Document.DocumentState.NOT_ISSSUED);
                documentRepository.save(document);
            }

        }



    }
}
